'use strict';

angular
    .module('app')
    .controller('LoginController', function($scope, $routeParams, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        $scope.login = function (form) {
            $auth.login(form)
                .then(function (response) {
                    $location.path('/teams');
                }, function(response) {
                    $scope.errors = response.data.messages;
                });
        };


    });