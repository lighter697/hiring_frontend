'use strict';

angular
    .module('app')
    .controller('ApplyController', function($scope, $http, $location, $routeParams, $auth, config) {

        $scope.form = {};


        if($routeParams.token) {

            $http({
                method: 'GET',
                url: config.base + '/api/v1/profile/get-invite',
                headers: {
                    'ApplyAuthorization': 'Bearer ' + $routeParams.token
                }
            }).then(function successCallback(response) {
                $scope.data = response.data.data;
                // console.log($scope.data);
            });
        }

        $scope.apply  = function(form) {
            $http({
                method: 'POST',
                url: config.base + '/api/v1/profile/apply',
                headers: {
                    'ApplyAuthorization': 'Bearer ' + $routeParams.token
                },
                data: form
            }).then(function successCallback(response) {

            });
        }



    });
