'use strict';

angular
    .module('app')
    .controller('DashboardController', function($scope, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        // $scope.calendar = {};

        $scope.index = function(callback) {
            $http({
                url: config.base + '/api/v1/calendar',
                method: "GET"
            }).then(function successCallback(response) {
                $scope.calendars = response.data.data;
                callback();
            });
        };

        $scope.index(function () {

            $scope.uiConfig = {
                calendar:{
                    height: 450,
                    editable: true,
                    header:{
                        left: 'title',
                        center: '',
                        right: 'today prev,next'
                    },
                    lazyFetching: false,
                    timezone: 'local',
                    events: {
                        type: 'GET',
                        url: config.base + '/api/v1/event',
                        data: {
                            calendars: function() {
                                return angular.toJson($scope.getActiveCalendars($scope.calendars))
                            }
                        },
                        headers: {
                            'Authorization': 'Bearer ' + $auth.getToken()
                        }
                    },
                    defaultView: $scope.calendarView,
                    nowIndicator: true,
                    eventClick: $scope.alertOnEventClick,
                    eventDrop: $scope.alertOnDrop,
                    eventResize: $scope.alertOnResize,
                    eventRender: $scope.eventRender
                }
            };
        });

        $scope.getActiveCalendars = function(calendars) {
            var arr = [];
            for(var i = 0; i < calendars.length; i++) {
                if(calendars[i].active) {
                    arr.push(calendars[i].id);
                }
            }

            return arr;
        };

        $scope.calendarViews = [
            {
                'title': 'Day',
                'name': 'agendaDay'
            },
            {
                'title': 'Week',
                'name': 'agendaWeek'
            },
            {
                'title': 'Month',
                'name': 'month'
            }
        ];

        $scope.calendarView = $scope.calendarViews[1].name;

        $scope.events = [];

        // $scope.changeCalendar = function(calendar) {
        //     $scope.calendar = calendar;
        //     console.log($scope.calendar);
        //     $scope.refetchEvents('_calendar');
        // };

        $scope.alertOnEventClick = function(model, jsEvent, view){
            $scope.updateEvent(model);
        };

        $scope.alertOnDrop = function(model, delta, revertFunc, jsEvent, ui, view){
            // console.log(model);
            $scope.save(model);
        };

        $scope.alertOnResize = function(model, delta, revertFunc, jsEvent, ui, view ){
            $scope.save(model);
        };

        $scope.addEvent = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/event/form.html',
                controller: 'CreateEventController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    profiles: function() {
                        return [];
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
                $scope.refetchEvents('_calendar');
            });
        };

        $scope.updateEvent = function(model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/event/form.html',
                controller: 'UpdateEventController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    model: function () {
                        return model
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
                $scope.refetchEvents('_calendar');
            });
        };

        $scope.addCalendar = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/calendar/form.html',
                controller: 'CreateCalendarController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    calendars: function () {
                        return $scope.calendars;
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index(function() {
                    $scope.calendar = $scope.calendars[0];
                });
                $scope.refetchEvents('_calendar');
            });
        };

        $scope.updateCalendar = function(calendar) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/calendar/form.html',
                controller: 'UpdateCalendarController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    calendars: function () {
                        return $scope.calendars;
                    },
                    calendar: function() {
                        return calendar
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index(function() {
                    $scope.calendar = calendar;
                });
                $scope.refetchEvents('_calendar');
            });
        };

        $scope.changeCalendarStatus = function(calendar){

            $http({
                url: "/api/v1/calendar/" + calendar.id,
                method: "PUT",
                data: calendar
            }).then(function successCallback(response) {
                $scope.refetchEvents('_calendar');
            });
        };


        $scope.changeView = function(view,calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
            $scope.calendarView = view;
        };

        $scope.renderCalender = function(calendar) {
            if(uiCalendarConfig.calendars[calendar]){
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        };

        $scope.refetchEvents = function(calendar) {
            if(uiCalendarConfig.calendars[calendar]){
                uiCalendarConfig.calendars[calendar].fullCalendar('refetchEvents');
            }
        };

        $scope.eventRender = function( event, element, view ) {
            element.attr({'tooltip': event.title,
                'tooltip-append-to-body': true});
            $compile(element)($scope);
        };

        $scope.save = function(form) {
            $http({
                url: "/api/v1/event/" + form.id,
                method: "PUT",
                data: form
            }).then(function() {
                $scope.refetchEvents('_calendar');
            });
        };

        $scope.eventSources = [$scope.events];



    });