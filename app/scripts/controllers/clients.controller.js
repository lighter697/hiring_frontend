'use strict';

angular.module('app')
    .controller('ClientsController', function ($scope, $http, $location, $uibModal) {

        $scope.filter = {
            page: 1,
            size: 10,
            deleteList: [],
            changeList: []
        };

        $scope.models = {};

        $scope.index = function (toTop) {
            $http({
                url: "/api/v1/client",
                method: "GET",
                params: $scope.filter
            }).then(function successCallback(response) {
                $scope.models = response.data.data;
                if (toTop === true) {
                    $('html, body').animate({scrollTop: 0}, 200);
                }
            });
        };

        $scope.index();

        $scope.create = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/client/form.html',
                controller: 'CreateClientController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {}
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        };

        $scope.update = function (model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/client/form.html',
                controller: 'UpdateClientController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    form: function () {
                        return model
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        };

        $scope.delete = function (model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/confirm.html',
                controller: 'DeleteClientController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    form: function () {
                        return model
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        }
    });
