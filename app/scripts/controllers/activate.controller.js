'use strict';

angular
    .module('app')
    .controller('ActivateController', function($scope, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        $http.get('/api/v1/auth/activate')
            .then(function(response) {
                $scope.messages = response.data.messages;
            }, function(response) {
                $scope.errors = response.data.messages;
            });
    });