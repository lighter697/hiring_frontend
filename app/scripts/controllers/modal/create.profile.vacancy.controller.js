'use strict';

angular.module('app').controller('CreateProfileVacancyController', function($scope, $http, $uibModalInstance, $uibModal, config, vacancy, profiles) {

    $scope.filter = {
        page: 1,
        size: 15,
        status: 'active'
    };

    $scope.title = 'Add profiles to vacancy.';
    $scope.text = 'Are you really want to add ' + profiles.length + ' profile(s) to this vacancy?';


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function () {
        for(var i = 0; i < profiles.length; i++) {
            (function(iteration) {
                // console.log(iteration + 1, profiles.length);
                $http({
                    url: config.base + "/api/v1/profile/add-vacancy",
                    method: "POST",
                    data: {
                        profile: profiles[iteration],
                        vacancy: vacancy.id
                    }
                }).then(function successCallback(response) {
                    if((iteration + 1) == profiles.length) {
                        $uibModalInstance.close(response.data.messages);
                    }
                }, function() {
                    if((iteration + 1) == profiles.length) {
                        $uibModalInstance.close(response.data.messages);
                    }
                });
            })(i);

        }
    }
});
