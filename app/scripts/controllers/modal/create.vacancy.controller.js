'use strict';

angular.module('app').controller('CreateVacancyController', function($scope, $http, $uibModalInstance, ClientService, $uibModal, config) {

    $scope.form = {};
    $scope.errors = {};
    $scope.clients = [];
    $scope.title = 'Create new vacancy';

    ClientService.getItems().then(function(result) {
        $scope.clients = result;
    });

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {

        var data = angular.copy(form);
        data.client = data.hasOwnProperty('client') ? data.client.id : null;

        $scope.errors = {};
        $http({
            url: config.base + '/api/v1/vacancy',
            method: "POST",
            data: data
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };


    $scope.new = function() {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/client/form.html',
            controller: 'CreateClientController',
            controllerAs: '$ctrl',
            size: 'md',
            resolve: {}
        });

        modalInstance.result.then(function (vacancies) {
            ClientService.getItems().then(function(result) {
                $scope.clients = result;
            });
        });
    }
});
