'use strict';

angular.module('app').controller('UpdateEventController', function($scope, $http, $uibModalInstance, $templateCache, config, model) {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var h = date.getHours();
    var min = date.getMinutes();

    $scope.form = model;

    $scope.errors = {};
    $scope.title = 'Update event';
    $scope.calendars = [];

    $scope.getCalendars = function() {
        $http({
            url: config.base + '/api/v1/calendar',
            method: "GET"
        }).then(function successCallback(response) {
            $scope.calendars = response.data.data;
        });
    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {
        $scope.errors = {};

        $http({
            url: "/api/v1/event/" + form.id,
            method: "PUT",
            data: form
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };

    $scope.endDateBeforeRender = endDateBeforeRender;
    $scope.endDateOnSetTime = endDateOnSetTime;
    $scope.startDateBeforeRender = startDateBeforeRender;
    $scope.startDateOnSetTime = startDateOnSetTime;

    function startDateOnSetTime () {
        $scope.$broadcast('start-date-changed');
    }

    function endDateOnSetTime () {
        $scope.$broadcast('end-date-changed');
    }

    function startDateBeforeRender ($dates) {
        if ($scope.form.end) {
            var activeDate = moment($scope.form.end);

            $dates.filter(function (date) {
                return date.localDateValue() >= activeDate.valueOf()
            }).forEach(function (date) {
                date.selectable = false;
            })
        }
    }

    function endDateBeforeRender ($view, $dates) {
        if ($scope.form.start) {
            var activeDate = moment($scope.form.start).subtract(1, $view).add(1, 'minute');

            $dates.filter(function (date) {
                return date.localDateValue() <= activeDate.valueOf()
            }).forEach(function (date) {
                date.selectable = false;
            })
        }
    }

    $scope.loadTags = function($query) {
        return $http({
            url: config.base + '/api/v1/profile/suggest',
            method: "GET",
            params: {
                phrase: $query,
                type: 'name'
            }
        }).then(function successCallback(response) {
            return response.data.data;
        });
    };

    $scope.delete = function(model) {
        if(confirm('Are you really want to delete this event?')) {
            $http({
                url: config.base + '/api/v1/event/' + model.id,
                method: "DELETE"
            }).then(function(response) {
                $uibModalInstance.close(response.data.data);
            });
        }
    }
});