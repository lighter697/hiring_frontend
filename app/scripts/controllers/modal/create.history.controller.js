'use strict';

angular.module('app').controller('CreateHistoryController', function($scope, $http, $uibModalInstance, filter, config) {


    $scope.errors = {};
    $scope.title = 'Add to history';
    $scope.form = {};

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function(form) {
        $scope.errors = {};

        $http({
            url: config.base + "/api/v1/history",
            method: "POST",
            data: {
                form: form,
                filter: filter,
                created_at: new Date().toLocaleString()
            }
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };

});