'use strict';

angular.module('app').controller('CreateCandidateController', function($scope, $http, $uibModalInstance) {

    $scope.form = {
        awards: [],
        educations: [],
        languages: [],
        profiles: [],
        references: [],
        skills: [],
        volunteers: [],
        works: []
    };
    $scope.errors = {};
    $scope.title = 'Create new candidate';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {
        $scope.errors = {};

        $http({
            url: "/api/v1/profile",
            method: "POST",
            data: form
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };

    $scope.add = function(arr) {
        arr.push({});
    };

    $scope.dropRow = function(arr, index) {
        arr.splice(index, 1);
    }
});