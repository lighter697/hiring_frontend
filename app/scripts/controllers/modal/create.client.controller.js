'use strict';

angular.module('app').controller('CreateClientController', function($scope, $http, $uibModalInstance) {

    $scope.form = {};
    $scope.errors = {};
    $scope.title = 'Create new client';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {
        $scope.errors = {};

        $http({
            url: "/api/v1/client",
            method: "POST",
            data: form
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    }
});