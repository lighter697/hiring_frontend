'use strict';

angular.module('app').controller('DeleteVacancyController', function($scope, $http, $uibModalInstance, form) {

    $scope.title = 'Delete vacancy';
    $scope.text = 'Are you really want to delete: ' + form.title + '?';
    $scope.model = form;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function(form) {
        $http({
            url: "/api/v1/vacancy/" + form.id,
            method: "DELETE"
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        });
    }
});