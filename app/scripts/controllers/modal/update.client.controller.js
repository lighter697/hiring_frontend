'use strict';

angular.module('app').controller('UpdateClientController', function($scope, $http, $uibModalInstance, form) {

    $scope.form = form;
    $scope.errors = {};
    $scope.title = 'Update client';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {
        $scope.errors = {};

        $http({
            url: "/api/v1/client/" + form.id,
            method: "PUT",
            data: form
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    }
});