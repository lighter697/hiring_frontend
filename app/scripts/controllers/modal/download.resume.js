'use strict';

angular.module('app').controller('DownloadResume', function($scope, $http, $uibModalInstance, $window, $auth, models, config) {

    $scope.title = 'Download resume(s)';
    $scope.text = 'Please make sure that you allowed multiple popup windows to be opened.';
    $scope.format = 'pdf';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function() {
        for(var i = 0; i < models.length; i++) {
            var url = config.base + '/api/v1/resume/download/' + models[i] + '?format=' + $scope.format + '&access_token=' + $auth.getToken();
            $window.open(url, '_blank');
        }
    }
});