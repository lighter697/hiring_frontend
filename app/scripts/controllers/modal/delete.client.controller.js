'use strict';

angular.module('app').controller('DeleteClientController', function($scope, $http, $uibModalInstance, form) {

    $scope.title = 'Delete client';
    $scope.model = form;
    $scope.text = 'Are you really want to delete '  + form.first_name + ' ' + form.last_name + ' from the list of your clients?';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function(model) {

        $http({
            url: "/api/v1/client/" + model.id,
            method: "DELETE"
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        });
    }
});