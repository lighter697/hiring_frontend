'use strict';

angular.module('app').controller('UpdateCalendarController', function($scope, $http, $uibModalInstance, calendars, calendar, $templateCache, config) {

    $scope.form = angular.copy(calendar);
    $scope.errors = {};
    $scope.title = 'New calendar';


    $scope.getTimezones = function() {
        $http({
            url: "/api/v1/calendar/timezones",
            method: "GET"
        }).then(function successCallback(response) {
            $scope.timezones = response.data.data;
            for(var i = 0; i < response.data.data.length; i++) {
                if(response.data.data[i].utc.indexOf($scope.form.timezone) !== -1) {
                    $scope.form.timezone = response.data.data[i];
                }
            }
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };

    $scope.getTimezones();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {
        $scope.errors = {};

        $http({
            url: "/api/v1/calendar/" + calendar.id,
            method: "PUT",
            data: form
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };

    $scope.delete = function(model) {
        if(confirm('Are you really want to delete this event?')) {
            $http({
                url: config.base + '/api/v1/calendar/' + model.id,
                method: "DELETE"
            }).then(function(response) {
                $uibModalInstance.close(response.data.data);
            });
        }
    }
});