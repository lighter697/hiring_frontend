'use strict';

angular.module('app').controller('DeleteProfileVacancyController', function($scope, $http, $uibModalInstance, $uibModal, profiles, vacancy, config) {

    $scope.title = 'Remove vacancy candidates.';

    $scope.text = 'Are you really want to remove ' + profiles.length + 'profiles from current vacancy?';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.ok = function () {
        for(var i = 0; i < profiles.length; i++) {
            (function(iteration) {
                // console.log(iteration, profiles.length);
                $http({
                    url: config.base + "/api/v1/profile/remove-vacancy",
                    method: "POST",
                    data: {
                        profile: profiles[iteration],
                        vacancy: vacancy.id
                    }
                }).then(function successCallback(response) {
                    if((iteration + 1) == profiles.length) {
                        $uibModalInstance.close(response.data.messages);
                    }
                }, function() {
                    if((iteration + 1) == profiles.length) {
                        $uibModalInstance.close(response.data.messages);
                    }
                });
            })(i);

        }
    };
});