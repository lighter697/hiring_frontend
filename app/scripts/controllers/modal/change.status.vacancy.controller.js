'use strict';

angular.module('app').controller('ChangeStatusVacancyController', function($scope, $http, $uibModalInstance, form, config) {

    $scope.title = 'Change Status';
    $scope.model = form;
    $scope.text = 'Are you really want to archive ' + form.title + '?';

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function(model) {
        $http({
            url: config.base + '/api/v1/vacancy/' + model.id,
            method: "PUT",
            data: model
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        });
    }
});
