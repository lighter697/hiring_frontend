'use strict';

angular.module('app').controller('CreateCalendarController', function($scope, $http, $uibModalInstance, calendars, $templateCache, config) {

    $scope.form = {

    };
    $scope.errors = {};
    $scope.title = 'New calendar';
    $scope.currentTimezone = jstz.determine().name();


    $scope.getTimezones = function() {
        $http({
            url: "/api/v1/calendar/timezones",
            method: "GET"
        }).then(function successCallback(response) {
            $scope.timezones = response.data.data;
            for(var i = 0; i < response.data.data.length; i++) {
                if(response.data.data[i].utc.indexOf($scope.currentTimezone) !== -1) {
                    $scope.form.timezone = response.data.data[i];
                }
            }
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };

    $scope.getTimezones();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function(form) {
        $scope.errors = {};

        $http({
            url: "/api/v1/calendar",
            method: "POST",
            data: form
        }).then(function successCallback(response) {
            $uibModalInstance.close(response.data.data);
        }, function(response) {
            $scope.errors = response.data.messages;
        });
    };
});