'use strict';

angular
    .module('app')
    .controller('AccountsController', function($scope, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        $http({
            method: 'GET',
            url: config.base + '/api/v1/auth/get-user'
        }).then(function(response) {
            $scope.user =  response.data.data;
            // console.log($scope.user);
        });


        $scope.login = function(account) {
            $auth.login({
                company: account.slug
            }, {
                url: config.base + '/api/v1/auth/login-from-token'
            })
                .then(function (response) {
                    $scope.user = response.data.data;
                    $location.path('/');
                }, function(response) {
                    $scope.errors = response.data.messages;
                });
        }


    });