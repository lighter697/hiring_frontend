'use strict';

angular
    .module('app')
    .controller('ForgotController', function($scope, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        $scope.forgot = function(form) {
            $http({
                url: config.base + "/api/v1/auth/forgot",
                method: "POST",
                data: form
            }).then(function successCallback(response) {
                // $location.path('/login');
            }, function(response) {
                // $location.path('/login');
            });
        }


    });