'use strict';

angular
    .module('app')
    .controller('EditTeamController', function($scope, $http, $location, $routeParams, uiCalendarConfig, config, $auth, $uibModal) {

        $scope.form = {};

        $scope.employees = [
            "1-5 people",
            "6-10 people",
            "11-20 people",
            "21-30 people",
            "31-40 people",
            "41-50 people"
        ];

        $scope.getTeam = function(id) {
            $http({
                method: 'GET',
                url: config.base + '/api/v1/team/' + id
            }).then(function(response) {
                $scope.form = response.data.data;
                // $scope.form.employees = response.data.data;
            }, function(response) {

            });
        };

        $scope.getTeam($routeParams.id);

        $scope.save = function(form) {
            $http({
                method: 'PUT',
                url: config.base + '/api/v1/team/' + form.id,
                data: form
            }).then(function(response) {
                $location.path('/teams');
            }, function(response) {
                $scope.errors = response.data.messages;
            });
        }

    });