'use strict';

angular
    .module('app')
    .controller('EmailController', function($scope, $http, $location, config, $routeParams) {

        $scope.form = {};
        $scope.errors = {};

        $scope.confirmEmail = function(form) {
            $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/confirm-email',
                data: form
            }).then(function(response) {
                $location.path('/confirm').search({
                    email: form.email,
                    redirect: $routeParams.redirect
                });
            }, function(response) {
                $scope.errors = response.data.messages;
            });
        };

    });