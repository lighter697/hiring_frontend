'use strict';

angular
    .module('app')
    .controller('PasswordController', function($scope, $auth, $http, $rootScope, $location, config, $routeParams) {

        $scope.form = $rootScope.user;
        $scope.errors = {};

        $scope.submit = function(form) {
            $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/password',
                data: form
            }).then(function(response) {
                $auth.logout();
                $location.path('/login');
            }, function(response) {
                $scope.errors = response.data.messages;
            });
        };

    });