'use strict';

angular
    .module('app')
    .controller('RegisterController', function($scope, $rootScope, $auth, $http, $location, config, $routeParams) {

        $scope.form = {};
        $scope.form = $rootScope.user;

        $scope.save = function (user) {
            $scope.errors = [];

            $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/create-user',
                data: user,
                headers: {
                    'Authorization': 'Bearer ' + $scope.form.token
                }
            }).then(function(response) {
                $auth.setToken(response.data.data.token);
                $location.path('/teams');
                $location.search({});
            }, function(response) {
                $scope.errors = response.data.messages
            });
        };

    });