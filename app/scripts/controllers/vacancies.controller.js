'use strict';

angular
    .module('app')
    .controller('VacanciesController', function($scope, $http, $location, $uibModal, $routeParams, config) {
        $scope.vacancies = {};

        $scope.animationsEnabled = true;

        $scope.filter = {
            page: 1,
            size: 10,
            status: $routeParams.status ? $routeParams.status : 'active',
            deleteList: [],
            changeList: []
        };

        $scope.index = function(toTop) {
            $http({
                url: config.base + '/api/v1/vacancy',
                method: "GET",
                params: $scope.filter
            }).then(function successCallback(response) {
                $scope.vacancies = response.data.data;

                if(toTop === true) {
                    $('html, body').animate({ scrollTop: 0 }, 200);
                }
            });
        };

        $scope.index();

        $scope.create = function () {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'views/vacancy/form.html',
                controller: 'CreateVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {}
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        };

        $scope.update = function (model) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/vacancy/form.html',
                controller: 'UpdateVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    form: function () {
                        return model;
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        };

        $scope.delete = function(model) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/confirm.html',
                controller: 'DeleteVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    form: function() {
                        return model
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        };

        $scope.deleteBulk = function(vacancies) {
            if(confirm('Are you really want to delete this vacancies?')) {
                for(var vacancy in vacancies) {
                    $http({
                        url: "/api/v1/vacancy/" + vacancies[vacancy].id,
                        method: "DELETE"
                    }).then(function successCallback(response) {
                        $scope.index();
                    });
                }
                $scope.checkedAll = false;
            }
        };

        $scope.changeStatus = function(vacancy, status) {

            var data = angular.copy(vacancy);
            data.status = status;
            data.client = vacancy.client.id;

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/confirm.html',
                controller: 'ChangeStatusVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    form: function() {
                        return data
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.index();
            });
        };

        $scope.changeStatusBulk = function(vacancies, status) {
            if(confirm('Are you really want to archive this vacancies?')) {
                for(var vacancy in vacancies) {

                    var data = angular.copy(vacancies[vacancy]);
                    data.status = status;
                    data.client = vacancies[vacancy].client.id;

                    $http({
                        url: config.base + '/api/v1/vacancy/' + data.id,
                        method: "PUT",
                        data: data
                    }).then(function successCallback(response) {
                        $scope.index();
                    });
                }
                $scope.checkedAll = false;
            }
        };

        $scope.getChecked = function () {
            var arr = [];

            for (var profile in $scope.vacancies.data) {
                if ($scope.vacancies.data[profile].checked) {
                    arr.push($scope.vacancies.data[profile]);
                }
            }

            return arr;
        };

        $scope.selectAll = function(all) {
            var arr = [];
            for (var profile in $scope.vacancies.data) {
                $scope.vacancies.data[profile].checked = all;
            }
            return arr;
        };

        $scope.open = function(vacancy) {
            $location.path('/search').search({vacancy: vacancy.id});
        }
    });
