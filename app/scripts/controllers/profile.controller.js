'use strict';

angular.module('app').controller('ProfileController', function($scope, $http, $routeParams, ProfileService, $uibModal, config, $window, $auth) {

    $scope.index = function() {
        ProfileService.getProfile($routeParams.id).then(function(result) {
            $scope.profile = result.data.data;
        });
    };

    $scope.index();

    $scope.getVacancies = function () {
        $http({
            url: config.base + '/api/v1/vacancy',
            method: "GET",
            params: {status: 'active'}
        }).then(function successCallback(response) {
            $scope.vacancies = response.data.data;
        });
    };

    $scope.getVacancies();


    $scope.addToVacancy = function (vacancy) {

        if(vacancy) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/confirm.html',
                controller: 'CreateProfileVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    vacancy: function() {
                        return vacancy
                    },
                    profiles: function() {
                        return [$scope.profile._id]
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {

            });
        }

    };

    $scope.removeFromVacancy = function (profile, vacancy) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/confirm.html',
            controller: 'DeleteProfileVacancyController',
            controllerAs: '$ctrl',
            size: 'md',
            resolve: {
                profiles: function () {
                    return [profile];
                },
                vacancy: function() {
                    return vacancy;
                }
            }
        });

        modalInstance.result.then(function (response) {
            $scope.index();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.formatDate = function (timePeriod) {
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var string = '';
        if (timePeriod.startDate) {
            if (timePeriod.startDate.month) {
                string = string + monthNames[timePeriod.startDate.month - 1] + ' ';
            } else {
                string = string + monthNames[0] + ' ';
            }
            if (timePeriod.startDate.year) {
                string = string + timePeriod.startDate.year + ' '
            }
        }
        if (timePeriod.endDate) {
            string = string + ' - ';
            if (timePeriod.endDate.month) {
                string = string + monthNames[timePeriod.endDate.month - 1] + ' ';
            } else {
                string = string + monthNames[0] + ' ';
            }
            if (timePeriod.endDate.year) {
                string = string + timePeriod.endDate.year + ' '
            }
        } else {
            string = string + ' - Now';
        }
        return string;
    };

    $scope.assignProfile = function (profiles) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/vacancy/profile-vacancy.html',
            controller: 'CreateProfileVacancyController',
            controllerAs: '$ctrl',
            size: 'md',
            resolve: {
                models: function () {
                    return profiles;
                }
            }
        });

        modalInstance.result.then(function (response) {

        });
    };

    $scope.getChecked = function (item) {
        var arr = [];
        if (item) {
            arr.push(item);
        }
        return arr;
    };

    $scope.resume = function(profile) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/search/download-resume.html',
            controller: 'DownloadResume',
            controllerAs: '$ctrl',
            size: 'md',
            resolve: {
                models: function () {
                    return [profile];
                }
            }
        });

        modalInstance.result.then(function (vacancies) {

        });
    };

    $scope.addToCalendar = function() {
        var data = angular.copy($scope.profile);
        data.profile = data._id;

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/event/form.html',
            controller: 'CreateEventController',
            controllerAs: '$ctrl',
            size: 'md',
            resolve: {
                profiles: function() {
                    return [data];
                }
            }
        });
    };
});
