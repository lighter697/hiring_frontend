'use strict';

angular
    .module('app')
    .controller('ConfirmController', function($scope, $http, $auth, $location, config, $routeParams) {

        $scope.form = {};
        $scope.errors = {};

        $scope.confirmCode = function(form) {
            $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/confirm-code',
                data: {
                    code: form.code,
                    email: $routeParams.email
                }
            }).then(function(response) {
                $auth.setToken(response.data.data.token);
                $location.search({});
                $location.path($routeParams.redirect ? $routeParams.redirect : '/teams');

            }, function(response) {
                $scope.errors = response.data.messages;
            });
        }

    });