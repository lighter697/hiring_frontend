'use strict';

angular
    .module('app')
    .controller('SearchController', function($scope, $http, $location, $timeout, $routeParams, $uibModal, toaster, $rootScope, config, $window, $auth) {


        $scope.extensionIsInstalled = !!document.getElementById('extension-is-installed');

        $scope.profiles = [];
        $scope.history = [];
        $scope.newSearch = true;

        // This property is used to fill the vacancy form.
        $scope.vacancy = false;
        $scope.vacancySelected = {};

        $scope.searchTab = 'ALL';
        $scope.conditions = [
            {id: '1', name: 'keywords', title: 'keywords'},
            {id: '2', name: 'education', title: 'education'},
            {id: '3', name: 'name', title: 'name'},
            {id: '4', name: 'experience', title: 'positions'},
            {id: '5', name: 'skills', title: 'skills'},
            {id: '6', name: 'location', title: 'location'},
            {id: '7', name: 'companies', title: 'companies'},
            {id: '8', name: 'language', title: 'language'}
        ];
        $scope.operators = [
            {id: '1', name: 'and'},
            {id: '2', name: 'or'}
        ];
        $scope.sortBy = [
            {
                type: 'relevant',
                field: false,
                order: false,
                title: 'Sort by most relevant'
            },
            {
                type: 'experience',
                field: 'dateOfFirstJob',
                order: 'ASC',
                title: 'Most experienced first'
            },
            {
                type: 'dateOfFirstJobAsc',
                field: 'dateOfFirstJob',
                order: 'DESC',
                title: 'Most experienced last'
            },
            {
                type: 'dateOfBirthAsc',
                field: 'dateOfBirth',
                order: 'DESC',
                title: 'Age from younger to older'
            },
            {
                type: 'dateOfBirthDesc',
                field: 'dateOfBirth',
                order: 'ASC',
                title: 'Age from older to younger'
            }
        ];
        var filter = {
            conditions: [],
            age: {
                min: 0,
                max: 60
            },
            experience: {
                min: 0,
                max: 20
            },
            dateOfLastJob: {
                min: 0,
                max: 10
            },
            sortBy: $scope.sortBy[0],
            job_duration: {
                min: '0',
                max: '60+'
            },
            languages: [],
            email: null,
            skype: null,
            phone: null,
            github: null,
            StackOverflow: null,
            page: 1,
            size: 15,
            // This property is used to store selected vacancy_id
            vacancy: null
        };
        $scope.filter = angular.copy(filter);
        $scope.filter.vacancy = $routeParams.vacancy ? angular.fromJson($routeParams.vacancy) : null;


        $scope.ageSlider = {
            min: 0,
            max: 60,
            options: {
                onEnd: function () {
                    $scope.updateResult();
                },
                showTicks: true,
                hideLimitLabels: true,
                hidePointerLabels: true,
                draggableRange: true,
                stepsArray: [
                    {value: 0, legend: '0'},
                    {value: 18, legend: '18'},
                    {value: 20, legend: '20'},
                    {value: 25, legend: '25'},
                    {value: 30, legend: '30'},
                    {value: 35, legend: '35'},
                    {value: 40, legend: '40'},
                    {value: 50, legend: '50'},
                    {value: 60, legend: '60+'}
                ]
            }

        };
        $scope.experienceSlider = {
            min: 0,
            max: 7,
            options: {
                onEnd: function () {
                    $scope.updateResult();
                },
                showTicks: true,
                hideLimitLabels: true,
                hidePointerLabels: true,
                stepsArray: [
                    {value: 0, legend: '0'},
                    {value: 1, legend: '1'},
                    {value: 2, legend: '2'},
                    {value: 3, legend: '3'},
                    {value: 4, legend: '4'},
                    {value: 5, legend: '5'},
                    {value: 10, legend: '10'},
                    {value: 15, legend: '15'},
                    {value: 20, legend: '20+'}
                ]
            }

        };

        $scope.dateOfLastJobSlider = {
            min: 0,
            max: 7,
            options: {
                onEnd: function () {
                    $scope.updateResult();
                },
                showTicks: true,
                hideLimitLabels: true,
                hidePointerLabels: true,
                stepsArray: [
                    {value: 0, legend: '0'},
                    {value: 1, legend: '1'},
                    {value: 2, legend: '2'},
                    {value: 3, legend: '3'},
                    {value: 4, legend: '4'},
                    {value: 5, legend: '5'},
                    {value: 10, legend: '10'},
                ]
            }

        };

        $scope.changeVacancy = function() {
            if($scope.vacancySelected) {
                $scope.filter.vacancy = $scope.vacancySelected.id;
                $location.path('/search').search({vacancy: $scope.filter.vacancy});
            } else {
                $scope.filter.vacancy = null;
                $location.path('/search').search({});
            }
        };

        $scope.getVacancies = function () {
            $http({
                url: config.base + '/api/v1/vacancy',
                method: "GET",
                params: {status: 'active'}
            }).then(function successCallback(response) {
                $scope.vacancies = response.data.data;
            });
        };

        $scope.getHistory = function () {
            $http({
                url: config.base + '/api/v1/history',
                method: "GET",
            }).then(function successCallback(response) {
                $scope.history = response.data.data;
            });
        };

        $scope.getVacancies();
        $scope.getHistory();

        $scope.prepareFilter = function () {
            var filter = angular.copy($scope.filter);
            var tmp = {};
            tmp.conditions = filter.conditions;
            tmp.age = {};
            if (filter.age.min !== 0) {
                tmp.age.min = filter.age.min;
            }
            if (filter.age.max !== 60) {
                tmp.age.max = filter.age.max;
            }
            tmp.experience = {};
            if (filter.experience.min !== 0 && filter.experience.max !== 20) {
                tmp.experience = {};
            }
            if (filter.experience.min !== 0) {
                tmp.experience.min = filter.experience.min;
            }
            if (filter.experience.max !== 20) {
                tmp.experience.max = filter.experience.max;
            }
            tmp.dateOfLastJob = {};
            if (filter.dateOfLastJob.min !== 0 && filter.dateOfLastJob.max !== 10) {
                tmp.dateOfLastJob = {};
            }
            if (filter.dateOfLastJob.min !== 0) {
                tmp.dateOfLastJob.min = filter.dateOfLastJob.min;
            }
            if (filter.dateOfLastJob.max !== 10) {
                tmp.dateOfLastJob.max = filter.dateOfLastJob.max;
            }
            if (filter.sortBy) {
                tmp.sortBy = filter.sortBy;
            }

            tmp.vacancy = filter.vacancy;
            tmp.page = filter.page;
            tmp.size = filter.size;

            tmp.email = filter.email;
            tmp.skype = filter.skype;
            tmp.phone = filter.phone;
            tmp.github = filter.github;
            tmp.StackOverflow = filter.StackOverflow;

            return tmp;
        };

        $scope.addCondition = function (filter) {
            filter.conditions.push({
                slug: angular.copy($scope.conditions[0]),
                operator: angular.copy($scope.operators[0]),
                tags: []
            });
        };

        $scope.assignCondition = function (filter, key, condition) {
            filter.conditions[key].slug = condition;
        };

        $scope.removeRow = function (filter, key, $event) {
            $event.stopPropagation();
            var conditions = [];
            angular.forEach(filter.conditions, function (value, index) {
                if (index !== key) {
                    conditions.push(value);
                }
            });
            filter.conditions = conditions;
        };


        $scope.removeAllRows = function () {
            $scope.filter.conditions = [];
        };

        $scope.isEmptyConditions = function () {
            var conditions = [];
            angular.forEach($scope.filter.conditions, function (value) {
                angular.forEach(value.tags, function (tag) {
                    conditions.push(tag);
                });
            });
            return !conditions.length;
        };

        $scope.flattenConditions = function() {
            var flatten = [];
            if($scope.filter.conditions.length) {
                for(var i = 0; i < $scope.filter.conditions.length; i++) {
                    if($scope.filter.conditions[i].tags.length) {
                        for(var n = 0; n < $scope.filter.conditions[i].tags.length; n++) {
                            flatten.push($scope.filter.conditions[i].tags[n]);
                        }
                    }
                }
            }

            return flatten;
        };

        $scope.getProfiles = function () {
            $http({
                cache: false,
                method: 'GET',
                url: config.base + '/api/v1/profile/search',
                params: {
                    query: angular.toJson($scope.prepareFilter())
                }
            }).then(function successCallback(response) {
                $scope.profiles = response.data;
            });
        };


        $scope.search = function () {
            $scope.filter = angular.copy($scope.newFilter);
            $scope.getProfiles();
            $scope.newSearch = false;
        };

        /**
         * Refetch profiles
         * @param item
         */
        $scope.updateResult = function (item) {
            $scope.getProfiles();
            $scope.checkedAll = false;
        };

        $scope.updateResult();

        /**
         * Loads tags for search
         * @param phrase
         * @param type
         */
        $scope.loadTags = function (phrase, type) {
            return $http({
                url: config.base + '/api/v1/profile/suggest',
                cache: true,
                method: "GET",
                params: {
                    phrase: phrase,
                    type: type.name
                }
            }).then(function successCallback(response) {
                return response.data.data;
            });

        };


        $scope.addToHistory = function () {
            // if(!$scope.isEmptyConditions()) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/history/add.html',
                    controller: 'CreateHistoryController',
                    controllerAs: '$ctrl',
                    size: 'md',
                    resolve: {
                        filter: function () {
                            return $scope.filter;
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    // $scope.updateResult();
                });
            // } else {
            //     toaster.pop({
            //         type: 'error',
            //         title: 'Error!',
            //         body: 'Nothing to add!!',
            //         timeout: 3000
            //     });
            // }

        };

        // $scope.clearHistory = function () {
        //     $scope.history = [];
        // };

        $scope.applyHistory = function (point) {
            $scope.filter = point;
            $scope.updateResult();
        };

        $scope.goToNewSearch = function() {
            $scope.filter = angular.copy(filter);
            $scope.updateResult()
        };

        $scope.isNewSearch = function() {
            return angular.toJson($scope.filter) == angular.toJson(angular.copy(filter));
        };

        /**
         * Saves vacancy
         * @param form
         */
        $scope.saveVacancy = function (form) {
            $http({
                url: config.base + '/api/v1/vacancy/' + form.id,
                method: "PUT",
                data: form
            });
        };


        /**
         * Removes profiles from vacancy
         * @param profiles
         * @param vacancy
         */
        $scope.removeFromVacancy = function (profiles, vacancy) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/confirm.html',
                controller: 'DeleteProfileVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    profiles: function () {
                        return profiles;
                    },
                    vacancy: function() {
                        return vacancy;
                    }
                }
            });

            modalInstance.result.then(function (response) {
                $scope.updateResult();
            });
        };

        /**
         * Add checked profiles to vacancy
         * @param vacancy
         */
        $scope.addToVacancy = function (vacancy) {

            if(vacancy) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/confirm.html',
                    controller: 'CreateProfileVacancyController',
                    controllerAs: '$ctrl',
                    size: 'md',
                    resolve: {
                        vacancy: function() {
                            return vacancy
                        },
                        profiles: function() {
                            return $scope.getChecked();
                        }
                    }
                });

                modalInstance.result.then(function (vacancies) {

                });
            }

        };

        /**
         * Updates vacancy
         * @param vacancy
         */
        $scope.updateVacancy = function (vacancy) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/vacancy/form.html',
                controller: 'UpdateVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    form: function () {
                        return vacancy;
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {
                $scope.getProfiles('vacancy fn');
                $scope.getVacancies();
            });
        };

        /**
         * Select all checked profiles
         * @returns {Array}
         */
        $scope.getChecked = function () {
            var arr = [];
            if($scope.profiles.hasOwnProperty('hits')) {

                for (var profile in $scope.profiles.hits.hits) {
                    if ($scope.profiles.hits.hits[profile]._source.checked) {
                        arr.push($scope.profiles.hits.hits[profile]._id);
                    }
                }

            }
            return arr;
        };

        $scope.getCheckedProfiles = function () {
            var arr = [];
            if($scope.profiles.hasOwnProperty('hits')) {

                for (var profile in $scope.profiles.hits.hits) {
                    if ($scope.profiles.hits.hits[profile]._source.checked) {
                        var prepare = $scope.profiles.hits.hits[profile]._source;
                        prepare.profile = $scope.profiles.hits.hits[profile]._id;
                        arr.push(prepare);
                    }
                }

            }
            return arr;
        };


        $scope.getTime = function(year) {
            var date = new Date(year, 1, 1);
            return date.getTime();
        };

        /**
         * Creates new vacancy
         */
        $scope.newVacancy = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/vacancy/form.html',
                controller: 'CreateVacancyController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {}
            });

            modalInstance.result.then(function (vacancies) {
                $scope.getVacancies();
            });
        };

        $scope.newCandidate = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/candidate/form.html',
                controller: 'CreateCandidateController',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {}
            });

            modalInstance.result.then(function (vacancies) {
                $scope.updateResult()
            });
        };

        /**
         * Let the user download resume or CV
         */
        $scope.resume = function() {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/search/download-resume.html',
                controller: 'DownloadResume',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    models: function () {
                        return $scope.getChecked();
                    }
                }
            });

            modalInstance.result.then(function (vacancies) {

            });
        };


        // ==================================================
        // Add to dashboard
        // ==================================================

        $scope.addToCalendar = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/event/form.html',
                controller: 'CreateEventController',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    profiles: function() {
                        return $scope.getCheckedProfiles();
                    }
                }
            });
        };

    });
