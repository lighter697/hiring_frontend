'use strict';

angular
    .module('app')
    .controller('SettingsController', function($scope, $routeParams, $http, $location, config) {

        // $scope.updateUser = function(form) {
        //     $scope.errors = [];
        //     UserService.updateUser(form).then(function(response) {
        //         $scope.user = response.data.data;
        //     }).catch(function(response) {
        //         $scope.errors  = response.data.messages;
        //     });
        // };

        $scope.updateUser = function (user) {
            $scope.errors = [];

            $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/update-user',
                data: {
                    first_name: user.first_name,
                    last_name: user.last_name,
                }
            }).then(function(response) {

            }, function(response) {
                $scope.errors = response.data.messages
            });
        };


    });