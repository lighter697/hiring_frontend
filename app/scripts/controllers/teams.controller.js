'use strict';

angular
    .module('app')
    .controller('TeamsController', function($scope, $rootScope, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        $rootScope.menu_items = [];

        $scope.login = function(team) {
            $auth.login({company: team.slug}, {
                url: config.base + '/api/v1/auth/login-from-token'
            })
                .then(function (response) {
                    // $location.path('/candidates');
                }, function(response) {
                    $scope.errors = response.data.messages;
                });
        };

        $scope.logged = function(team_id) {
            return $auth.getPayload().team === team_id;
        };


    });