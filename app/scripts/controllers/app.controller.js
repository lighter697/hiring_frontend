'use strict';

angular
  .module('app')
  .controller('AppController', AppController);

AppController.$inject = ['$rootScope', '$http', '$location', '$auth', 'toaster', 'UserService', 'AppService', '$rootScope', '$uibModal'];

function AppController ($scope, $http, $location, $auth, toaster, UserService, AppService, $rootScope, $uibModal)
{
    $scope.location = $location;
    // $scope.user = $rootScope.user;
    $scope.token = $auth.getToken();

    //
    // $rootScope.menu_items = [
    //     {
    //         path: '/dashboard',
    //         text: 'Dashboard'
    //     },
    //     {
    //         path: '/candidates',
    //         text: 'Candidates'
    //     },
    //     {
    //         path: '/vacancies',
    //         text: 'Vacancies'
    //     },
    //     {
    //         path: '/clients',
    //         text: 'Clients'
    //     }
    // ];

    $scope.$on('loading:error', function (event, args){
        var mapAndAlert = function(object){
            for(var key in object){
                if (typeof object[key]  === 'string'){
                    toaster.pop({
                        type: 'error',
                        title: 'Some error occured',
                        body: object[key],
                        timeout: 3000
                    });
                } else mapAndAlert(object[key]);
            }
        };
        mapAndAlert(args);
    });

    $scope.$on('loading:success', function(event, args){
        var mapAndAlert = function(object){
            for(var key in object){
                if (typeof object[key]  === 'string'){
                    toaster.pop({
                        type: 'success',
                        title: 'Success!',
                        body: object[key],
                        timeout: 3000
                    });
                } else mapAndAlert(object[key]);
            }
        };
        mapAndAlert(args);
    });

    $scope.$on('logout', function (){
        $location.path('/login');
    });

    $scope.logout = function () {
        $auth.logout();
        localStorage.removeItem('user');
        $location.path('/login');
    };

    $scope.social = AppService.getSocialAssoc;

    $scope.getClass = function(link) {
        link = $scope.getHost(link);
        for(var index in $scope.social) {
            if(link.indexOf(index) !== -1) {
                return $scope.social[index];
            }
        }

        return 'fa-link';
    };

    $scope.extractHostname = function(url) {
        var hostname;
        if (url.indexOf("://") > -1) {
            hostname = url.split('/')[2];
        } else {
            hostname = url.split('/')[0];
        }
        hostname = hostname.split(':')[0];

        return hostname;
    };

    $scope.getHost = function(url) {
        var domain = $scope.extractHostname(url),
            splitArr = domain.split('.'),
            arrLen = splitArr.length;
        if (arrLen > 2) {
            domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        }
        return domain;
    };


    $scope.colors = [
        '#F44336',
        '#E91E63',
        '#9C27B0',
        '#673AB7',
        '#3F51B5',
        '#2196F3',
        '#03A9F4',
        '#00BCD4',
        '#009688',
        '#4CAF50',
        '#8BC34A',
        '#CDDC39',
        '#FFEB3B',
        '#FFC107',
        '#FF9800',
        '#FF5722',
        '#795548',
        '#9E9E9E',
        '#607D8B',
        '#000000'
    ];
}
