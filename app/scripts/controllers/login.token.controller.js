'use strict';

angular
    .module('app')
    .controller('LoginTokenController', function ($scope, $routeParams, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        if ($routeParams.token) {
            $auth.setToken($routeParams.token);

            $location.path($routeParams.redirect ? $routeParams.redirect : '/teams');
        }

    });