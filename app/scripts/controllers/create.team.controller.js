'use strict';

angular
    .module('app')
    .controller('CreateTeamController', function($scope, $http, $location, $compile, uiCalendarConfig, config, $auth, $uibModal) {

        $scope.employees = [
            "1-5 people",
            "6-10 people",
            "11-20 people",
            "21-30 people",
            "31-40 people",
            "41-50 people"
        ];

        $scope.save = function(form) {
            $http({
                method: 'POST',
                url: config.base + '/api/v1/team',
                data: form
            }).then(function(response) {
                $location.path('/teams');
            }, function(response) {
                $scope.errors = response.data.messages;
            });
        }

    });