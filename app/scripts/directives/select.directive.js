'use strict';

angular
    .module('app')
    .directive('dropdown', function() {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            ngModel: '=',
            items: '=',
            itemPlaceholder: '=',
            itemClass: '=',
            ngChange: '&'
        },
        link: function(scope, element, attrs, ngModelCtrl) {

            // element.on('click', function(event) {
            //     event.preventDefault();
            // });

            if(!scope.itemClass) {
                scope.itemClass = 'btn-gray';
            }

            scope.select = function(item) {
                ngModelCtrl.$setViewValue(item);
            };

            function updateView(value) {
                ngModelCtrl.$viewValue = value;
                ngModelCtrl.$render();
            }

            function updateModel(value) {
                ngModelCtrl.$modelValue = value;
                scope.ngModel = value; // overwrites ngModel value
            }
        },
        templateUrl: 'views/templates/dropdown-template.html'
    };
});
