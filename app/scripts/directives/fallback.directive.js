'use strict';

angular
    .module('app')
    .directive('bgImage', function () {
        return {
            link: function(scope, element, attr) {

                scope.defaultImage = '/images/noimage.png';

                attr.$observe('bgImage', function() {
                    if (!attr.bgImage) {
                        // No attribute specified, so use default
                        element.css("background-image","url("+scope.defaultImage+")");
                    } else {
                        var image = new Image();
                        image.src = attr.bgImage;
                        image.onload = function() {
                            //Image loaded- set the background image to it
                            element.css("background-image","url("+attr.bgImage+")");
                            // element.css("background-image","url("+scope.defaultImage+")");
                        };
                        image.onerror = function() {
                            //Image failed to load- use default
                            element.css("background-image","url("+scope.defaultImage+")");
                        };
                    }
                });
            }
        };
    });