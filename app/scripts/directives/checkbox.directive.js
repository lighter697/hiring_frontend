'use strict';

angular
    .module('app')
    .directive('checkbox', function () {
        return {
            restrict: 'AE',
            scope: {
                ngCheckboxStatus: '='
            },
            link: function(scope, element, attr) {},
            template: '<div class="hr-checkbox" ng-class="{checked: ngCheckboxStatus}" ng-click="ngCheckboxStatus = !ngCheckboxStatus"><i class="icon ion-android-done"></i></div>'
        };
    });