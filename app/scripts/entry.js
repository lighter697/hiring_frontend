'use strict';

angular.module(
    'app',
    [
        'ng',
        'ngRoute',
        'rzModule',
        'ngTagsInput',
        'ui.bootstrap',
        'satellizer',
        'toaster',
        'ngAnimate',
        'angular-loading-bar',
        'vcRecaptcha',
        'ngSanitize',
        'yaru22.angular-timeago',
        'simplemde',
        'ui.calendar',
        'angularMoment',
        'ui.bootstrap.datetimepicker',
        'ui.dateTimeInput'
    ]
)

    .config(function ($routeProvider, $locationProvider, $authProvider, $httpProvider, cfpLoadingBarProvider, tagsInputConfigProvider, $qProvider, config) {

        $qProvider.errorOnUnhandledRejections(false);
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.parentSelector = '.loader-container';
        $authProvider.loginUrl = config.base + '/api/v1/auth/login';
        $authProvider.tokenRoot = 'data';
        $authProvider.tokenName = 'token';
        $authProvider.storageType = 'localStorage';
        tagsInputConfigProvider.setActiveInterpolation('tagsInput', {placeholder: true});
        $httpProvider.useApplyAsync(true);

        $routeProvider.when('/403', {
            templateUrl: 'views/403.html',
            data: {
                permissions: {
                    only: []
                }
            }
        });


        $routeProvider.when('/login', {
            templateUrl: 'views/login.html',
            reloadOnSearch: false,
            data: {
                redirectTo: {
                    path: '/register',
                    params: {}
                },
                permissions: {
                    only: ['guest']
                }
            }
        });

        $routeProvider.when('/forgot', {
            templateUrl: 'views/forgot.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['guest']
                }
            }
        });

        $routeProvider.when('/teams', {
            templateUrl: 'views/teams.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['logged', 'admin']
                }
            }
        });

        $routeProvider.when('/teams/create', {
            templateUrl: 'views/team/create.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['logged', 'admin']
                }
            }
        });

        $routeProvider.when('/teams/edit/:id', {
            templateUrl: 'views/team/edit.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['logged', 'admin']
                }
            }
        });

        $routeProvider.when('/email', {
            templateUrl: 'views/email.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['guest']
                }
            }
        });

        $routeProvider.when('/confirm', {
            templateUrl: 'views/confirm-email.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['guest']
                }
            }
        });

        // ==============================================================================
        // |
        // |
        // |
        // |
        // ==============================================================================

        $routeProvider.when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            reloadOnSearch: false,
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['admin']
                }
            }
        });

        $routeProvider.when('/candidates', {
            templateUrl: 'views/candidates.html',
            reloadOnSearch: false,
            data: {
                redirectTo: {
                    path: '/login',
                    params: {}
                },
                permissions: {
                    only: ['admin']
                }
            }
        });

        $routeProvider.when('/profile/:id', {
            templateUrl: 'views/profile.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['admin']
                }
            }
        });

        $routeProvider.when('/vacancies', {
            templateUrl: 'views/vacancy/index.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['admin']
                }
            }
        });

        $routeProvider.when('/clients', {
            templateUrl: 'views/client/index.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['admin']
                }
            }
        });

        $routeProvider.when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['admin']
                }
            }
        });

        $routeProvider.when('/password', {
            templateUrl: 'views/password.html',
            data: {
                redirectTo: {
                    path: 'login',
                    params: {}
                },
                permissions: {
                    only: ['logged']
                }
            }
        });


        $routeProvider.when('/settings', {
            templateUrl: 'views/settings.html',
            data: {
                redirectTo: {
                    path: '/login',
                    params: {}
                },
                permissions: {
                    only: ['logged', 'admin']
                }
            }
        });

        $routeProvider.otherwise("/teams");

        $locationProvider.html5Mode(true);

        $httpProvider.interceptors.push(function ($q, $rootScope) {
            return {
                'response': function (response) {
                    if (response.data) if (response.data.messages) if (Object.keys(response.data.messages).length > 0) $rootScope.$broadcast('loading:success', response.data.messages);
                    return response || $q.when(response);
                },
                'responseError': function (response) {
                    if (response.data) if (response.data.messages) if (Object.keys(response.data.messages).length > 0) $rootScope.$broadcast('loading:error', response.data.messages);
                    return $q.reject(response);
                }
            };
        });
    })


    .constant('config', require('../../config.json'))
    .constant('moment', require('moment-timezone'))

    .run(
        function ($rootScope, $location, $auth, $http, config) {

            $rootScope.validateRoles = function (roles, next_route, current_route) {

                if(next_route.hasOwnProperty('$$route')) {
                    // console.log(next_route.$$route.data);

                    if (next_route.$$route.data.hasOwnProperty('permissions')) {

                        if (next_route.$$route.data.permissions.hasOwnProperty('only')) {

                            var only = [];

                            for (var o = 0; o < roles.length; o++) {
                                if (next_route.$$route.data.permissions.only.indexOf(roles[o]) !== -1) {
                                    only.push(roles[o]);
                                }
                            }


                            if (only.length == 0 && next_route.$$route.data.permissions.only.length > 0) {
                                $location.path(next_route.$$route.data.redirectTo.path);
                                $location.search(next_route.$$route.data.redirectTo.params);
                            }
                        }
                    }
                }


            };

            $rootScope.$on('$routeChangeStart', function (event, next, current) {

                if ($auth.isAuthenticated()) {
                    $http({
                        method: 'GET',
                        url: config.base + '/api/v1/auth/get-user'
                    }).then(function (response) {
                        $rootScope.user = response.data.data;
                        $rootScope.validateRoles($rootScope.user.roles, next, current);
                    });
                } else {
                    console.log('hello');
                    $rootScope.validateRoles(['guest'], next, current);
                }

                // switch (user_type) {
                //     case 'user':
                //         $http({
                //             method: 'GET',
                //             url: config.base + '/api/v1/auth/get-user'
                //         }).then(function (response) {
                //             $rootScope.user = response.data.data;
                //             $rootScope.validateRoles($rootScope.user.roles, next, current);
                //         });
                //         break;
                //     case 'account':
                //         $http({
                //             method: 'GET',
                //             url: config.base + '/api/v1/auth/get-account'
                //         }).then(function (response) {
                //             $rootScope.user = response.data.data;
                //             $rootScope.validateRoles($rootScope.user.roles, next, current);
                //         });
                //         break;
                //     default:
                //         $rootScope.validateRoles(['guest'], next, current);
                // }

            });

        }
    );

// SERVICES
require('./services/user.service');
require('./services/profile.service');
require('./services/app.service');
require('./services/client.service');

// CONTROLLERS
require('./controllers/app.controller');
require('./controllers/search.controller');
require('./controllers/vacancies.controller');
require('./controllers/dashboard.controller');
require('./controllers/activate.controller');
require('./controllers/clients.controller');
require('./controllers/profile.controller');
require('./controllers/apply.controller');
require('./controllers/register.controller');
require('./controllers/login.controller');
require('./controllers/login.token.controller');
require('./controllers/teams.controller');
require('./controllers/create.team.controller');
require('./controllers/edit.team.controller');
require('./controllers/forgot.controller');
require('./controllers/settings.controller');
require('./controllers/email.controller');
require('./controllers/confirm.controller');
require('./controllers/password.controller');

// MODAL CONTROLLERS
require('./controllers/modal/create.vacancy.controller');
require('./controllers/modal/update.vacancy.controller');
require('./controllers/modal/create.client.controller');
require('./controllers/modal/update.client.controller');
require('./controllers/modal/delete.client.controller');
require('./controllers/modal/delete.vacancy.controller');
require('./controllers/modal/change.status.vacancy.controller');
require('./controllers/modal/create.profile.vacancy.controller');
require('./controllers/modal/delete.profile.vacancy.controller');
require('./controllers/modal/download.resume');
require('./controllers/modal/create.event.controller');
require('./controllers/modal/update.event.controller');
require('./controllers/modal/create.calendar.controller');
require('./controllers/modal/update.calendar.controller');
require('./controllers/modal/create.history.controller');
require('./controllers/modal/create.candidate.controller');

// DIRECTIVES
require('./directives/fallback.directive');
require('./directives/checkbox.directive');
require('./directives/debounce.directive');
require('./directives/outside.click.directive');
require('./directives/select.directive');
