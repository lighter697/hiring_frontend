'use strict';

angular.module('app').factory('ClientService', ['$http', 'config', function ($http, config) {

    return {
        getItems: function() {
            return $http({
                url: config.base + '/api/v1/client',
                method: "GET"
            }).then(function successCallback(response) {
                return response.data.data;
            });
        }
    };
}]);
