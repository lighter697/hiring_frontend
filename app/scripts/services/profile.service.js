'use strict';

angular.module('app').factory('ProfileService', ['$http', 'config', function ($http, config) {

    return {
        getProfile: function(id) {
            return $http.get(config.base + '/api/v1/profile/show/' + id);
        }
    };
}]);
