'use strict';

angular.module('app').factory('UserService', ['$http', 'config', function ($http, config) {

    return {
        activateUser: function(token) {
            return $http.get('/api/v1/auth/activate-user/' + token);
        },

        createUser: function(user) {
            return $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/create-user',
                data: user
            });
        },

        getAccount: function () {
            return $http({
                method: 'GET',
                url: config.base + '/api/v1/auth/get-account'
            }).then(function(response) {
                return response.data;
            });
        },

        getUser: function () {
            return $http({
                method: 'GET',
                url: config.base + '/api/v1/auth/get-user'
            }).then(function(response) {
                return response.data;
            });
        },

        updateUser: function(form) {
            return $http({
                method: 'POST',
                url: config.base + '/api/v1/auth/update-user',
                data: form
            });
        }
    };
}]);
