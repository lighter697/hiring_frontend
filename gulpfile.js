'use strict';

var gulp = require('gulp'),
    jade = require('jade'),
    modRewrite = require('connect-modrewrite'),
    webpack = require('webpack-stream'),
    sass = require('gulp-sass');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'del', 'browser-sync']
});

// Constants
var BUILD_DIR = 'build/';

// Webpack
gulp.task('webpack:vendor', function() {
    return gulp.src('app/scripts/vendor.js')
        .pipe(webpack({
            output: {
                filename: "vendor.js"
            }
        }))
        .pipe(gulp.dest(BUILD_DIR + 'scripts/'))
});

gulp.task('webpack', function() {
    return gulp.src('app/scripts/entry.js')
        .pipe(webpack({
            module: {
                loaders: [
                    { test: /\.json$/, loader: "json-loader" }
                ]
            },
            output: {
                filename: "app.js"
            }
        }))
        .pipe(gulp.dest(BUILD_DIR + 'scripts/'))
        .pipe($.browserSync.reload({stream:true}));
});

// Views
gulp.task('jade:dev', function(){
    return gulp.src('app/views/**/*.jade')
        .pipe($.jade({
            jade: jade,
            pretty: true
        }))
        .pipe(gulp.dest(BUILD_DIR + 'views'));
});

// Html
gulp.task('html:dev', ['jade:dev'], function() {
    return gulp.src(BUILD_DIR + 'views/index.html')
        .pipe(gulp.dest(BUILD_DIR))
        .pipe($.browserSync.reload({stream:true}));
});

// Sass
gulp.task('sass', function () {
    gulp.src([
        'app/styles/app.scss'
    ], { sourcemap: true })
        .pipe(sass().on('error', function (err) {
            console.error('Error!', err.message);
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(BUILD_DIR + 'styles'))
        .pipe($.browserSync.reload({stream:true}));

    gulp.src([
        'app/styles/print/*.scss'
    ], { sourcemap: true })
        .pipe(sass().on('error', function (err) {
            console.error('Error!', err.message);
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(BUILD_DIR + 'styles/print'))
        .pipe($.browserSync.reload({stream:true}));
});

// Sass vendor
gulp.task('sass:vendor', function () {
    gulp.src('app/styles/vendor.scss', { sourcemap: true })
        .pipe(sass().on('error', function (err) {
            console.error('Error!', err.message);
        }))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(BUILD_DIR + 'styles'))
        .pipe($.browserSync.reload({stream:true}));
});

// Images
gulp.task('images', function () {
    return gulp.src('app/images/**/*')
        .pipe(gulp.dest(BUILD_DIR + 'images/'))
        .pipe($.browserSync.reload({stream:true}));
});

// Fonts
gulp.task('fonts', function () {

    gulp.src([
        "node_modules/bootstrap/fonts/*"
    ]).pipe(gulp.dest(BUILD_DIR + 'fonts/bootstrap'));

    gulp.src([
        "node_modules/ionicons/fonts/*"
    ]).pipe(gulp.dest(BUILD_DIR + 'fonts/ionicons'));

    gulp.src([
        'app/fonts/*'
    ]).pipe(gulp.dest(BUILD_DIR + 'fonts/'));
});

// .htaccess
gulp.task('htaccess', function () {
    return gulp.src('.htaccess')
        .pipe(gulp.dest(BUILD_DIR));
});

// Static server
gulp.task('serve:dev', ['dev'], function() {
    $.browserSync({
        proxy: "hiring.dev"

        // server: {
        //     baseDir: [".", BUILD_DIR, "app"],
        //     middleware: [
        //         modRewrite([
        //             '!\\.html|\\.js|\\.css|\\.png|\\.jpg|\\.gif|\\.svg|\\.txt|\\.otf|\\.eot|\\.ttf|\\.woff|\\.woff2 /index.html [L]'
        //         ])
        //     ]
        // }
    });
});


// e2e tests
gulp.task('webdriver-update', $.protractor.webdriver_update);
gulp.task('protractor', ['webdriver-update'], function () {
    gulp.src(['test/e2e/**/*.js'])
        .pipe($.protractor.protractor({
            configFile: "test/protractor.config.js",
            args: ['--baseUrl', 'http://localhost:3000']
        }))
        .on('error', function (e) {
            throw e
        });
});

// Clean
gulp.task('clean', function () {
    $.del.sync([BUILD_DIR + '*']);
});

// Development
gulp.task('dev', ['clean', 'webpack', 'webpack:vendor', 'sass:vendor', 'sass', 'images', 'fonts', 'html:dev']);

// Default Task (Dev environment)
gulp.task('default', ['serve:dev'], function() {
    // Scripts
    gulp.watch(['config.json', 'app/scripts/**/*.js'], ['webpack']);

    // Views
    gulp.watch([
        'app/views/**/*.jade'
    ], ['html:dev', 'jade:dev']);

    // .pipe($.jadeFindAffected())
    // .pipe($.jade({jade: jade, pretty: true}))
    // .pipe(gulp.dest(BUILD_DIR + 'views'));

    // // Htmls
    // gulp.watch([
    //     BUILD_DIR + 'views/*.html',
    //     BUILD_DIR + 'views/**/*.html'
    // ], ['html:dev']);

    // Styles
    gulp.watch(['app/styles/**/*.scss'], ['sass']);
});
